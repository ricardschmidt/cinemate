import axios from 'axios';

const api = axios.create({
    baseURL: import.meta.env.VITE_TMDB_API,
	params: {
		api_key: import.meta.env.VITE_TMDB_KEY,
		language: import.meta.env.VITE_LANGUAGE,
	}
})

export default api;
