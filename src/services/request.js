export const FILMS = "films";
export const GENRES = "genres";
export const CART_ITEMS = "cartItems";
export const FAV_ITEMS = "favItems";

export const getFilms = () => JSON.parse(localStorage.getItem(FILMS));

export const setFilms = films => {
	localStorage.setItem(FILMS, JSON.stringify(films));
};

export const getGenres = () => JSON.parse(localStorage.getItem(GENRES));

export const setGenres = genres => {
	localStorage.setItem(GENRES, JSON.stringify(genres));
};

export const getCartItems = () => JSON.parse(localStorage.getItem(CART_ITEMS));

export const setCartItems = cartItems => {
	localStorage.setItem(CART_ITEMS, JSON.stringify(cartItems));
};

export const getFavItems = () => JSON.parse(localStorage.getItem(FAV_ITEMS));

export const setFavItems = favItems => {
	localStorage.setItem(FAV_ITEMS, JSON.stringify(favItems));
};
