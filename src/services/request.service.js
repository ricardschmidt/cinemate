import axios from './axios';
import { setFilms, setGenres } from './request'
import colors from '../utils/colors'

class MoviesService {
	trendingDay() {
		return axios.get('/trending/movie/day')
		.then(response => {
			if (response.data.results) {
				let results = response.data.results
				results.forEach(result => {
					result.value = result.genre_ids.length > 0 ? colors[`id${result.genre_ids[0]}`].price : 29.9
				});
				setFilms(results);
				return(results)
			}
		})
	}

	searchByName(name) {
		return axios.get('/search/movie', {
			params: {
				query: name
			}
		})
		.then(response => {
			if (response.data.results) {
				let results = response.data.results
				results.forEach(result => {
					result.value = result.genre_ids.length > 0 ? colors[`id${result.genre_ids[0]}`].price : 29.9
				});
				setFilms(results);
				return(results)
			}
		})
	}

	getGenres() {
		return axios.get('/genre/movie/list')
		.then(response => {
			if (response.data) {
				setGenres(response.data.genres);
			}
		})
	}
}
export default new MoviesService();
