import RequestService from '../services/request.service';
import { getFilms, getGenres, getCartItems, setCartItems, getFavItems, setFavItems } from '../services/request'

const films = getFilms();
const genres = getGenres();
const cartItems = getCartItems()
const favItems = getFavItems()

const genresInitialState = genres
  ? { status: { hasGenres: true }, genres }
  : { status: { hasGenres: false }, genres: null };

const filmsInitialState = films
  ? { status: { hasFilms: true }, films }
  : { status: { hasFilms: false }, films: null };

const cartInitialState = cartItems
		? { status: { hasCartItems: true }, cartItems }
		: { status: { hasCartItems: false }, cartItems: [] };

const favInitialState = favItems
		? { status: { hasFavItems: true, isSearch: false }, favItems }
		: { status: { hasFavItems: false, isSearch: false }, favItems: [] };

export const request = {
	namespaced: true,
	state: genresInitialState,
	actions: {
		requestGenres({ commit }) {
			return RequestService.getGenres()
			.then(() => {
					commit('setGenresSuccess', getGenres());
					return Promise.resolve(getGenres());
				},
			);
		},
	},
	mutations: {
		setGenresSuccess(state, genres) {
			state.status.hasGenres = true;
			state.genres = genres;
		},
		setGenresFailure(state) {
			state.status.hasGenres = false;
			state.genres = null;
		},
		removeGenres(state) {
			state.status.hasGenres = false;
			state.genres = null;
		},
	}
};

export const film = {
	namespaced: true,
	state: filmsInitialState,
	actions: {
		getTrends({ commit }) {
			return RequestService.trendingDay()
			.then((films) => {
					commit('setFilmsSearchSuccess', films);
					return Promise.resolve(films);
				},
			);
		},

		searchByName({ commit }, name) {
			return RequestService.searchByName(name)
			.then((films) => {
					commit('setFilmsSuccess', films);
					return Promise.resolve(films);
				},
			);
		},
	},
	mutations: {
		setFilmsSuccess(state, films) {
			state.status.hasFilms = true;
			state.status.isSearch = false;
			state.films = films;
		},

		setFilmsSearchSuccess(state, films) {
			state.status.hasFilms = true;
			state.status.isSearch = true;
			state.films = films;
		},
	}
};

export const cart = {
	namespaced: true,
	state: cartInitialState,
	actions: {
		addItem({ commit, getters }, item) {
			commit('addItem', { item, getters });
			return Promise.resolve(item);
		},
		removeItem({ commit }, item) {
			commit('removeItem', item);
			return Promise.resolve(item);
		},
		removeAllItem({ commit }) {
			commit('removeCartItems');
			return Promise.resolve();
		},
	},
	mutations: {
		setCartItemsSuccess(state, cartItems) {
			state.status.hasCartItems = true;
			state.cartItems = cartItems;
			setCartItems(state.cartItems)
		},
		addItem(state, {item, getters}) {
			if(getters.getCartById(item.id))
			return

			state.status.hasCartItems = true;
			state.cartItems.push(item);
			setCartItems(state.cartItems)
		},
		removeItem(state, item) {
			let i = state.cartItems.indexOf(item)
			state.cartItems.splice(i, 1);
			state.status.hasCartItems = state.cartItems.length > 0 ? true : false;
			setCartItems(state.cartItems)
		},
		setCartItemsFailure(state) {
			state.status.hasCartItems = false;
			state.cartItems = [];
			setCartItems(state.cartItems)
		},
		removeCartItems(state) {
			state.status.hasCartItems = false;
			state.cartItems = []
			setCartItems(state.cartItems)
		},
	},
	getters: {
		getCartById: (state) => (id) => {
			return state.cartItems.find( item => item.id === id)
		}
	}
};

export const favorite = {
	namespaced: true,
	state: favInitialState,
	actions: {
		addItem({ commit }, item) {
			commit('addItem', item);
			return Promise.resolve(item);
		},
		removeItem({ commit }, item) {
			commit('removeItem', item);
			return Promise.resolve(item);
		},
		removeAllItem({ commit }) {
			commit('removeFavItems');
			return Promise.resolve();
		},
	},
	mutations: {
		addItem(state, item) {
			state.status.hasFavItems = true;
			state.favItems.push(item);
			setFavItems(state.favItems)
		},
		removeItem(state, item) {
			let i = state.favItems.indexOf(item)
			state.favItems.splice(i, 1);
			state.status.hasFavItems = state.favItems.length > 0 ? true : false;
			setFavItems(state.favItems)
		},
		removeFavItems(state) {
			state.status.hasFavItems = false;
			state.favItems = []
			setFavItems(state.cartItems)
		},
	},
	getters: {
		getFavById: (state) => (id) => {
			return state.favItems.find( item => item.id === id)
		}
	}
};
