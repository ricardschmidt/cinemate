import { createStore } from 'vuex'
import { request, cart, favorite, film } from './request.module';

export default new createStore({
	modules: {
		film,
		request,
		cart,
		favorite,
	}
})
