import { createApp } from 'vue'
import App from './App.vue'

import maska from 'maska'

import router from "./router/router";
import store from './store';

import './index.css'
import '@/assets/scss/index.scss';


createApp(App)
.use(router)
.use(store)
.use(maska)
.mount('#app')
