const colors = {
	"id28": {
		bgColor: "bg-blue-100",
		textColor: "text-blue-800",
		price: 29.90
	},
	"id12": {
		bgColor: "bg-indigo-100",
		textColor: "text-indigo-800",
		price: 39.90
	},
	"id16": {
		bgColor: "bg-yellow-100",
		textColor: "text-yellow-800",
		price: 49.90
	},
	"id35": {
		bgColor: "bg-green-100",
		textColor: "text-green-800",
		price: 59.90
	},
	"id80": {
		bgColor: "bg-violet-100",
		textColor: "text-violet-800",
		price: 69.90
	},
	"id99": {
		bgColor: "bg-sky-100",
		textColor: "text-sky-800",
		price: 79.90
	},
	"id18": {
		bgColor: "bg-purple-100",
		textColor: "text-purple-800",
		price: 89.90
	},
	"id10751": {
		bgColor: "bg-cyan-100",
		textColor: "text-cyan-800",
		price: 99.90
	},
	"id14": {
		bgColor: "bg-pink-100",
		textColor: "text-pink-800",
		price: 19.90
	},
	"id36": {
		bgColor: "bg-amber-100",
		textColor: "text-amber-800",
		price: 29.90
	},
	"id27": {
		bgColor: "bg-gray-100",
		textColor: "text-gray-800",
		price: 39.90
	},
	"id10402": {
		bgColor: "bg-emerald-100",
		textColor: "text-emerald-800",
		price: 49.90
	},
	"id9648": {
		bgColor: "bg-teal-100",
		textColor: "text-teal-800",
		price: 59.90
	},
	"id10749": {
		bgColor: "bg-red-100",
		textColor: "text-red-800",
		price: 69.90
	},
	"id878": {
		bgColor: "bg-orange-100",
		textColor: "text-orange-800",
		price: 79.90
	},
	"id10770": {
		bgColor: "bg-lime-100",
		textColor: "text-lime-800",
		price: 89.90
	},
	"id53": {
		bgColor: "bg-fuchsia-100",
		textColor: "text-fuchsia-800",
		price: 99.90
	},
	"id10752": {
		bgColor: "bg-gose-100",
		textColor: "text-gose-800",
		price: 19.90
	},
	"id37": {
		bgColor: "bg-stone-100",
		textColor: "text-stone-800",
		price: 29.90
	},
}

export default colors
