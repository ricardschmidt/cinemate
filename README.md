# Cinemate in Vue 3 + Vite

Aplicação para venda de filmes online

## Implementar uma lojinha de filmes com funcionalidade básica.

Requisitos para Dev Pleno:

- [ ] Implementar o layout da página inicial da loja
- [ ] Você deverá utilizar o framework Vue.js
- [ ] Vuex para gerenciamento de estado da loja
- [ ] integração com a API de filmes TMDb
- [ ] Lista de filmes retornadas da API
- [ ] Pesquisa de filmes
- [ ] Carrinho de compras lateral totalmente funcional
- [ ] Mostrar quantidade de itens do carrinho no botão de abrir o carrinho
- [ ] Prosseguir para o checkout
- [ ] Validar o preenchimento completo do formulário
- [ ] Ao finalizar a compra, apresentar modal de sucesso
- [ ] Implementar máscaras nos campos de email, celular, CEP e CPF
- [ ] Implemente a funcionalidade de adicionar/remover filmes a lista de favoritos do usuário.

## Funcionalidades

- Ao acessar o site é feita uma requisião para API do TMDb com as trends do dia.
- São exibidos cards com os TOP 20 filmes do dis
- Os cards exibem o cartaz do filme, generos, nota valor e botão para adicionar ao carrinho
- Ao clicar no nome é possível ver mais informação sobre o filme, como tempo de duração e sinopse.
- Botão para favoritar o filme
- Só é possível adicionar o filme uma vez ao carrinho.
- No menu contem, barra de pesquisa, botão de favoritos e do carrinho informando número de itens no carrinho e favoritados.
- Clicando nos botões de favorito e carrinho abre um sidecard com informações dos itens.
- No carrinho é possível remover todos os itens ou um a um e finalizar o pedido.
- Nos favoritos é possivel remover dos favoritos e aidiconar ao carrinho.
- No input do menu basta digitar o nome do filme para realziar uma nova busca que será mostrada nos cards.
- Na página de checkout tem um formulário con válidação dos campos e informação sobre os itens do carrinho e valor total da compra.
- Os campos de CPF, Celular e CEP tem mascará.
- Ao finalizar abre um modal de agradecimento o carrinho é limpo e o usuário redirecionado para home.

## Quick start

Quick start options:

- Clone the repo: `git clone https://gitlab.com/ricardschmidt/cinemate.git`.
- [Download from Gitlab](https://gitlab.com/ricardschmidt/cinemate/-/archive/main/cinemate-main.zip).
- Run `npm install` or `yarn install`
- Run `npm run dev` or `yarn dev` to start a local development server


## Tecnologias usadas

[Vue.js (3)](https://vuejs.org/) as framework for development.
[Vue CLI 5.0.4](https://github.com/vuejs/vue-cli) for project scaffolding.
[Vue Router](https://router.vuejs.org/) for handling routes.
[Vuex](https://vuex.vuejs.org/) for state.
[Tailwind 3](https://tailwindcss.com/) as a general css foundation.
[Headless UI](https://headlessui.dev/) for some complex js components such as dialog, transitionChild.
[Maska](https://github.com/beholdr/maska) for input mask.

## File Structure

Within the download you'll find the following directories and files:

```
Vue Challenge
|-- src
	|-- App.vue
	|-- main.js
	|-- index.css
	|-- assets
	|   |-- scss
	|-- components
	|   |-- Cards
	|-- layout
	|-- router
	|-- service
	|-- store
	|-- views
```

## Reference
Projeto feito como avaliaão técnica para DOT digital group